% This simple example illustrates the use of the main functions to check
% partial identifiability of an Exact NMF. 
clear all; clc; 

Install; 

C = [1 0 3; 0 1 2; 0 2 1; 3 2 1]
S = eye(3) 

disp('Check partial identifiability of columns of C using Theorem 6-7-8:'); 
disp('K = check_partial_identif_C(C,S)')
K = check_partial_identif_C(C,S)
disp('The first column of C satisfies Theorem 6 and is identifiable.')
disp('The third is not since its support contains the support of the other 2 columns.')
disp('The second cannot be garanteed to be identifiable using Theorems 7 and 8.'); 
disp('*****************************') 
disp('Press any button to continue.') 
disp('*****************************') 
pause; 

disp('Check partial identifiability of columns of C using Theorem 10 (specialized for r=3).'); 
disp('K = check_partial_identif_C_requals3(C,S)')
K = check_partial_identif_C_requals3(C,S)
disp('The first two columns of C satisfy Theorem 9 and hence are identifiable.')

y = 1; n = 0; 
b = input('Do you want to visualize the geometric interpretation (the NPP)? (y/n)\n');
if ~isempty(b) && (b == 'y' || b == 1)
    % Figure default
    set(0, 'DefaultAxesFontSize', 25);
    set(0, 'DefaultLineLineWidth', 2);
    [P,U,V] = NPPrank3matrix(C*S');  
end

disp('*****************************') 
disp('Press any button to continue.') 
disp('*****************************') 
pause; 
disp('Check partial identifiability of columns of C AND S using Theorems 6-7-8-9:'); 
disp('[K,L] = check_partial_identif(C,S)')
[K,L] = check_partial_identif(C,S) 
disp('No column of S can be garanteed to be identifiable with our Theorems,'); 
disp('since none satisfies the selective window assumption (that is, no row');
disp('of C is a multiple of a unit vector).'); 
