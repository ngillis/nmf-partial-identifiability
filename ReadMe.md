This code implements the conditions under which columns of C and S in an exact NMF CS^T are identifiable; see the paper
Partial Identifiability for Nonnegative Matrix Factorization, by Nicolas Gillis and Robert Rajko, accepted in SIAM J. on Matrix Analysis and Applications, 2022; see https://arxiv.org/abs/2206.08022 

More precisely: 
- check_partial_identif_C.m checks whether the conditions of Theorems 6-7-8 are satisfied for C. 
- check_partial_identif_C_requals3.m checks whether the conditions of Theorem 9 are satisfied for C. 
- check_partial_identif.m checks whether the conditions of Theorems 6-7-8 or of Theorem 9 are satisfied for C and S. 

RunMe.m provides a simple example to use these functions. 

The folder "Examples" contains the numerical examples from the paper, and an additional example in the case r=3. 

The code also contains a function to display the geometric interpretation of Exact NMF when r=3, namely NPPrank3matrix.m; this code is borrowed from the toolbox 
https://gitlab.com/ngillis/nmfbook/ which implements various NMF methods presented in the book 'Nonnegative Matrix Factorization', SIAM, 2020, by N. Gillis; see https://sites.google.com/site/nicolasgillis/book
