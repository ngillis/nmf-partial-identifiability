% Chech partial identifiability of C in the Exact NMF R = CS^T combining
% Theorem 6 (DBU theorem), Theorem 7 (based on the intersection of minimal 
% facets) and Theorem 8 (using partial identifiability sequentially). 
% 
% This is Algorithm 1 in the paper 
% 
% Partial Identifiability for Nonnegative Matrix Factorization, 
% Nicolas Gillis and Robert Rajko, 2022. 
% 
% Input : (C,S) Two nonnegative matrices with r columns, C and S, so that 
%         CS^T is an Exact NMF of the matrix R = CS^T. 
%         K : set of columns which are already guaranteed to be unique
%             default: K = []. 
% Output: K = set of identifiable columns of C

function K = check_partial_identif_C(C,S,K) 
%% Chech input 
r = size(C,2); 
if size(S,2) ~= r
    error('C and S must have the same number of columns.'); 
end
if min(S(:)) < 0 || min(C(:)) < 0
    error('C and S must be nonnegative.');
end
if rank(S) < r || rank(C) < r
    error('C and S must have full column rank.');
end
if nargin <= 2
    K = []; 
end
%% Preprocessing: normalization of the columns of C and S^T to unit l1 norm
[C,S] = normalizel1(C,S); 
%% Identify columns of C satisfying the selective window condition  
SW = selectwindow_support(C,S);    
%% Theorems 6/7 only apply to columns of C in SW
SW = setdiff(SW,K); % remove columns that we know are already identifiable 
if isempty(SW);
    return; 
end  
%% First precompute columns of R not containing the supports of columns of C
R = C*S'; 
[m,n] = size(R); 
% Compute Ksupp{k} = { i | R(:,i) does not contain supp(C(:,k)) } 
for i = 1 : r
    suppCi = find(C(:,i) > 0); 
    if length(suppCi) == 0
        Ksupp{i} = [1:n];
    else
        if length(suppCi) > 1
            suppCi = min(R(suppCi,:)); 
        end
        Ksupp{i} = find( suppCi > 0 ); 
        Ksupp{i} = setdiff([1:n],Ksupp{i}); 
    end
end 
%% Apply Theorems 6 and 7 only to columns of C in SW
for i = 1 : length(SW) 
    k = SW(i); 
    %% Use of Theorem 6 (easier to check) 
    I = find(C(:,k) == 0); 
    if rank(C(I,[1:k-1 k+1:r])) == r-1
        K = [K, k];
    else
        %% Use of Theorem 7 (if Theorem 6 fails)
        % Identify the columns of R(:,j) such that 
        % F_C(C(:,k) do not intersect F_C(R(:,j))
        J = []; 
        candidateJ = Ksupp{k}; j = 1; 
        while j <= length(candidateJ) 
            if computinterFkFj(C,k,R(:,candidateJ(j))) == 0
                J = [J, candidateJ(j)];
            end
            if length(J) >= r-1 && rank(R(:,J)) == r-1 
                K = [K, k];
                j = length(candidateJ); % return the while loop
            end
            j = j+1; 
        end
    end
end
%%  Combine Theorems 6 and 7 with Theorem 8 for p=1,2,... 
% Use recursion: call check_partial_identif_C, 
% this is Theorem 8 with p=1 
toaddtoK = []; 
i = 1; 
while i <= length(K) % consider C(:,K(i)) already identified (p=1)
    Kbar = setdiff([1:r],K(i));
    [a,b] = intersect(Kbar,K);   
    if rank(S(Ksupp{K(i)},Kbar)) == length(Kbar)
        Krec = check_partial_identif_C(C(:,Kbar),S(Ksupp{K(i)},Kbar),b);
        toaddtoK = [toaddtoK, setdiff(Kbar(Krec),K)]; 
        K = unique([K toaddtoK]); 
    end
    i = i+1; 
end