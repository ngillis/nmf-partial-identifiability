% Chech partial identifiability of C in the Exact NMF R = CS^T using the
% Theorem 10 for r=3 in 
% 
% Partial Identifiability for Nonnegative Matrix Factorization, 
% Nicolas Gillis and Robert Rajko, 2022. 
% 
% Input : Two nonnegative matrices with r columns, C and S, so that CS^T is
%         an Exact NMF of the matrix R = CS^T. 
%         K : set of columns which are already guaranteed to be unique
%             default: K = [] 
% Output: K = set of identifiable columns of C

function K = check_partial_identif_C_requals3(C,S) 

r = size(C,2); 
if size(S,2) ~= r
    error('C and S must have the same number of columns.'); 
end
if min(S(:)) < 0 || min(C(:)) < 0
    error('C and S must be nonnegative.');
end
if rank(S) < r || rank(C) < r
    error('C and S must have full column rank.');
end
K = []; 
% Normalization 
[C,S] = normalizel1(C,S); 
R = C*S'; 
% Identify columns of C satisfying the selective window assumption and
% whose support does not contain that of other columns of C 
SW = selectwindow_support(C,S); 
% Theorem 10 only applies to columns of C in SW
if length(SW) <= 1 
    return; 
end 
% Use of Theorem 10: pick all pairs of columns in SW 
check2by2 = nchoosek(SW,2); 
for i = 1 : size(check2by2,1); 
    kj = check2by2(i,:); 
    k = kj(1); 
    j = kj(2); 
    interFkFj = computinterFkFj(C,k,C(:,j)); 
    % Compute the vertices of F_C(C(:,k)) and F_C(C(:,j))
    vertk = verticessegment(C,k); 
    vertj = verticessegment(C,j); 
    if interFkFj == 0 % F_C(C(:,k)) and F_C(C(:,j)) do not intersect 
        % Check whether there exist R(:,l) such that 
        % R(:,l) not in conv( F_C(C(:,k)), C(:,j) ), and 
        Hk = nonnegativeleastsquares([vertk C(:,j)],R); 
        errk = sum( (R - [vertk C(:,j)]*Hk).^2 ); 
        % R(:,l) not in conv( F_C(C(:,j)), C(:,k) )
        Hj = nonnegativeleastsquares([vertj C(:,k)],R); 
        errj = sum( (R - [vertj C(:,k)]*Hj).^2 ); 
        errjk = sqrt( min(errk,errj) ); 
    else % F_C(C(:,k)) and F_C(C(:,j)) do intersect 
        % Check whether there exist R(:,l) such that 
        % R(:,l) not in conv( F_C(C(:,k)), F_C(C(:,j)) )  
        Hkj = nonnegativeleastsquares([vertk vertj],R); 
        errjk = sqrt(sum( (R - [vertk vertj]*Hkj).^2 )); 
    end
    if max(errjk) > 1e-6
            K = unique([K, k,j]);
    end
    if length(K) == 3;
        return;
    end
end