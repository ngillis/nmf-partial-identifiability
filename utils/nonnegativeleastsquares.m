% Given A and B, solves the following nonnegative least squares
%           min_{X >= 0} ||AX - B||_F^2 
% using lsqnonneg.m that solves the problem by column, since the above
% problem is equivalent to independent NNLS problems 
%           min_{X(:,i) >= 0} ||AX(:,i) - B(:,i)||_F^2 

function X = nonnegativeleastsquares(A,B) 

[m,n] = size(B); 
[m,r] = size(A); 
X = zeros(r,n); 
for i = 1 : n
    X(:,i) = lsqnonneg(A,B(:,i));
end