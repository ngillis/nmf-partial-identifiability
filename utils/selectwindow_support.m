% Identify columns of C satisfying the selective window assumption and
% whose support does not contain the support of other columns. 
% 
% Input : Two nonnegative matrices with r columns, C and S, so that CS^T is
%         an Exact NMF of the matrix R = CS^T. 
% Output: SW = set of columns of C satisfying the selective window assumption 
%         and whose support does not contain the support of other columns 

function SW = selectwindow_support(C,S) 

% normalize columns of C and S^T so that they become column stochastic
[C,S] = normalizel1(C,S); 
% find columns of S multiple of unit vectors 
SW = find( max(S) == 1 ); 
% Remove indices in SW corresponding to columns whose support contain the
% support of other columns 
removefromSW = []; 
for i = 1 : length(SW) 
    k = SW(i); 
    for j = 1 : size(C,2) 
        % Check if supp(C(:,j)) in supp(C(:,k)) for some j ~= k
        if j~=k && (nnz(C(:,j)) == nnz(C(:,j).*C(:,k)))
            removefromSW = [removefromSW, k]; 
            break; 
        end
    end
end 
SW = setdiff(SW,removefromSW); 

%% Note: The two above procedures (normalization and checking support 
%% containement) are useless within check_partial_identif_C.m 
%% that already performs these tasks. However, it makes selectwindow_support.m 
%% usable outside check_partial_identif_C.m. 