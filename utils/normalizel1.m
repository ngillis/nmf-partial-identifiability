% Given an Exact NMF CS^T, normalize the columns of C and rows of S so that
% C and S^T become column stochastic. 

function [C,S] = normalizel1(C,S)

C = C./repmat(max(eps,sum(C)),size(C,1),1); 
S = S./repmat(max(eps,sum(S')'),1,size(S,2)); 