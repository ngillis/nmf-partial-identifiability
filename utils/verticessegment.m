% Given a nonnegative m-by-3 matrix C, and an index k, normalize the
% columns so that it becomes colum stocastic, then compute the vertices of 
% F_C(C(:,k)) = { X = Cz | Cz >= 0, z^Te = 1, (Cz)_i = 0 when C(i,k) = 0} 
% WHEN F_C(C(:,k)) is a segment or a vertex. 

function V = verticessegment(C,k) 

[m,r] = size(C); 
if min(C(:)) < 0
    error('C must be nonnegative.');
end
if r ~= 3
    error('C must have three columns.'); 
end
C = C./repmat(sum(C),size(C,1),1); 
Kkj = find(C(:,k)==0); 
c = zeros(r,1); 
for i = 1 : length(Kkj)
    c = c + C(Kkj(i),:)'; 
end 
randir = randn(r,1); 
options = optimoptions('linprog','Display','none'); 
z1 = linprog(randir,-C,zeros(m,1),[ones(1,r); c'],[1; 0],[],[],[],options); 
V(:,1) = C*z1; 
z2 = linprog(-randir,-C,zeros(m,1),[ones(1,r); c'],[1; 0],[],[],[],options); 
if norm(z1-z2) > 1e-6 % F_C(C(:,k)) could be a vertex  
    V(:,2) = C*z2; 
end