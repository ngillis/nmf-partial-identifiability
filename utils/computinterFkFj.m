% Given a nonnegative matrix C, checks whether F_C(C(:,k)) and F_C(Rj) 
% intersect, where C and Rj have been normalized to be column stochastic. 
% Recall that  
%   F_C(C(:,k)) = {x | x = Cz >= 0, supp(x) in supp(C(:,k))}; see 
% 
% Partial Identifiability for Nonnegative Matrix Factorization, 
% Nicolas Gillis and Robert Rajko, 2022. 
% 
% Input : A nonnegative matrix C, and index k, a column Rj
% Output: interFkFj = 1 if F_C(C(:,k)) and F_C(Rj) intersect, 
%                   = 0 otherwise. 

function interFkFj = computinterFkFj(C,k,Rj); 

if min(C(:)) < 0
    error('C must be nonnegative.');
end
[m,r] = size(C); 
if length(Rj) ~= m
    error('Rj must have the same dimension as the columns of C.');
end
if r < k
    error('k must be a valid index for a column of C.');
end
% Normalize C and Rj
Rj = Rj/sum(Rj); 
C = C./repmat(max(eps,sum(C)),m,1); 
% Solve linear program min_z sum(z(Kkj)) s.t. Cz >= 0 and e^T z = 1
Kkj = unique([find(C(:,k)==0); find(Rj==0)]); 
f = zeros(r,1); 
for i = 1 : length(Kkj)
    f = f + C(Kkj(i),:)'; 
end 
options = optimoptions('linprog','Display','none');
z = linprog(f,-C,zeros(m,1),ones(1,r),1,[],[],[],options); 
% F_C(C(:,k)) and F_C(Rj) intersect if f'*z = 0 
if f'*z < 1e-6 % threshold value 
    interFkFj = 1; 
else
    interFkFj = 0; 
end
