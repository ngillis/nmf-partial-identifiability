% Chech partial identifiability of the Exact NMF R = CS^T using 
% Theorems 6,7,8 and 9 on C and S; see 
% 
% Partial Identifiability for Nonnegative Matrix Factorization, 
% Nicolas Gillis and Robert Rajko, 2022. 
% 
% Input : Two nonnegative matrices with r columns, C and S, so that CS^T is
%         an Exact NMF of the matrix R = CS^T. 
%         
% Output: K = set of identifiable columns of C
%         L = set of identifiable columns of S

function [K,L] = check_partial_identif(C,S) 

r = size(C,2); 
K = check_partial_identif_C(C,S);    
if r == 3
    K3 = check_partial_identif_C_requals3(C,S);    
    K = union(K,K3);
end
if length(K) == r % Lemma 1: if C is fully identifiable and rank(C)=r, S is too
    L = 1:r;
    return; 
else % Apply on S symmetrically 
    L = check_partial_identif_C(S,C);   
    if r == 3
        L3 = check_partial_identif_C_requals3(S,C);   
        L = union(L,L3);
    end
end
    if length(L) == r % Lemma 1  
        K = 1:r;
    end
end