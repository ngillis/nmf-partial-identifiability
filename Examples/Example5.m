% Example 5 from the paper
% Partial identifiability for Nonnegative Matrix Factorization, 
% Nicolas Gillis and Robert Rajko, 2022. 
clear all; clc;
addpath('.\')
C = [ 0 1 1 1
      0 1 2 3
      0 1 2 1
      1 0 1 2
      1 0 2 1
      1 1 0 1
      1 1 1 0 ]
S = eye(4)
disp('The columns of C garanteed to be identifiable by Theorems 6,7,8 are:');
K = check_partial_identif_C(C,S)