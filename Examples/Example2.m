% Example 2 from the paper
% Partial identifiability for Nonnegative Matrix Factorization, 
% Nicolas Gillis and Robert Rajko, 2022. 
clear all; clc;
C = [2 2 2; 
     1 3 1; 
     1 1 3; 
     0 2 2; 
     0 1 2]
S = eye(3)
disp('The columns of C garanteed to be identifiable by Theorems 6,7,8 are:');
K = check_partial_identif_C(C,S)