% Example 3 from the paper
% Partial identifiability for Nonnegative Matrix Factorization,
% Nicolas Gillis and Robert Rajko, 2022.
clear all; clc;
C = [0 1 1; 1 0 0; 0.5 0 1; 0.5 1 0]
S = [1 0 0; 0 0.8 0.2; 0 0.2 0.8]' 
disp('The columns of C garanteed to be identifiable by Theorems 6,7,8 are:');
K = check_partial_identif_C(C,S)

y = 1; n = 0; 
b = input('Do you want to visualize the geometric interpretation (the NPP)? (y/n)\n');
if ~isempty(b) && (b == 'y' || b == 1)
    [P,U,V] = NPPrank3matrix(C*S');  
end
