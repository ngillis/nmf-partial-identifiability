% This is an example not present in our paper
% It illustrate a case where Theorems 6-7-8 do not provide identifiability
% while Theorem 9 does. 

clear all; clc; 
C = [0 1 0.5 0.5; 0.5 0.5 0 1; 0.5 0.5 1 0]'
S = eye(3)

disp('No column can be garanteed to be identifiale using Theorems 6-7-8:')
disp('K = check_partial_identif_C(C,S)')
K = check_partial_identif_C(C,S)

disp('However, all pairs of columns of C satisfy Theorem 10:')
disp('K = check_partial_identif_C_requals3(C,S)')
K = check_partial_identif_C_requals3(C,S)

y = 1; n = 0; 
b = input('Do you want to visualize the geometric interpretation (the NPP)? (y/n)');
if ~isempty(b) && (b == 'y' || b == 1)
    [P,U,V] = NPPrank3matrix(C*S');  
end