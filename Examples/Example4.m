% Example 3 from the paper
% Partial identifiability for Nonnegative Matrix Factorization,
% Nicolas Gillis and Robert Rajko, 2022.
clear all; clc;
C = [0 1 0 1
     0 0 1 1
     0.75 0.25 0.25 0.75
     1 0 1 0
     1 1 0 0
     0.25 0.75 0.75 0.25]
S = eye(4)
disp('The columns of C garanteed to be identifiable by Theorems 6,7,8 are:');
K = check_partial_identif_C(C,S)