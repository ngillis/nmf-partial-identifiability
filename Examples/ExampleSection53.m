% Example from the paper (Section 4.3): 
% Lakeh, M.A., Abdollahi, H., Rajko, R.: Predicting the uniqueness of single 
% non-negative profiles estimated by multivariate curve resolution methods. 
% Analytica Chimica Acta p. 339575 (2022). 
% See Section 5.3 in 
% Partial identifiability for Nonnegative Matrix Factorization, 
% Nicolas Gillis and Robert Rajko, 2022. 
close all; clear all; clc; 

load Data5compChromatSyst; 

% If you want to use a subset of the components: 
% K = [1 3 4]; 
% Cw5c = Cw5c(:,K); 
% Sw5c = Sw5c(:,K); 

[m,r] = size(Cw5c); 
figure; 
plot([1:m]+400,Cw5c); hold on; 
[r,n] = size(Sw5c'); 

axis([401 400+m 0 max(Cw5c(:))]); 
xlabel('Retention times', 'Interpreter', 'latex'); 
ylabel('Concentration', 'Interpreter', 'latex'); 
title('Elution profiles', 'Interpreter', 'latex'); 
h = legend('A', 'B', 'C', 'D', 'E'); 
set(h,'Interpreter','latex'); 

figure; 
plot(Sw5c); 
xlabel('Wavelength', 'Interpreter', 'latex'); 
ylabel('Absorbance', 'Interpreter', 'latex'); 
title('Spectra', 'Interpreter', 'latex'); 

axis([1 204 0 1.5]); 
h = legend('A', 'B', 'C', 'D', 'E');
set(h,'Interpreter','latex'); 

disp('Checking partial identifiability:') 

[K,L] = check_partial_identif(Cw5c,Sw5c)