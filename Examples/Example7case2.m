% Example 7 (case 2) from the paper
% Partial identifiability for Nonnegative Matrix Factorization, 
% Nicolas Gillis and Robert Rajko, 2022. 
clear all; clc;
C = [0     1   0.5  0.5; 
     0.5  0.5  0    1; 
     0.75 0.25 0.75 0.25]'
S = eye(3)
K = check_partial_identif_C_requals3(C,S)

y = 1; n = 0; 
b = input('Do you want to visualize the geometric interpretation (the NPP)? (y/n)\n');
if ~isempty(b) && (b == 'y' || b == 1)
    [P,U,V] = NPPrank3matrix(C*S');  
end
